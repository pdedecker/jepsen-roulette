# Jepsen Roulette

Every YouTube link now has a 1 in 3 chance of being Carly Rae Jepsen's I Really Like You. (Please prank responsibly and be kind to others!)

## Install
Get it for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/jepsen-roulette/) or [Chrome](https://chrome.google.com/webstore/detail/ccdldgnpagkhaldmbmpbijdipkpllebc).

## Attribution
App icon used from [noto-emoji](https://github.com/googlei18n/noto-emoji) under an Apache License 2.
