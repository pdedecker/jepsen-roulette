function examineUrl(requestDetails) {
  // This is our target video.
  const targetID = "qV5lzRHrGeg";

  const matches = /^https?:\/\/(?:www\.)?youtube\.com\/watch\?v=([^&]+)/.exec(requestDetails.url);

  // If the URL doesn't match the regex pattern or we've already redirected,
  // don't touch the request.
  if (!matches || matches[1] == targetID)
    return;

  console.debug("Matched: " + matches[1]);

  // YouTube does some fancy JavaScript magic behind the scenes where it fires off an XHR for a URL that
  // a main_frame change was already fired off for. Cancelling this request increases performance without
  // breaking the site.
  if (requestDetails.type == "xmlhttprequest") {
    console.debug("Swallowing XHR...");
    return {cancel: true};
  }

  const shouldRedirect = Math.random() < 1/3;
  if (!shouldRedirect) {
    console.debug("No redirect.");
    return;
  }

  console.debug("Redirecting...");
  return {
    redirectUrl: "https://www.youtube.com/watch?v=" + targetID
  };
}

browser.webRequest.onBeforeRequest.addListener(
  examineUrl,
  {
    // For performance reasons, do prefiltering that's as specific as possible without
    // causing false negatives
    urls: ["*://*.youtube.com/watch*"],
    // We need to match xmlhttprequest as well to avoid an XHR overwriting our redirect
    types: ["main_frame", "xmlhttprequest"]
  },
  ["blocking"]
);
